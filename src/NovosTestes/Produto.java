/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NovosTestes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

public class Produto extends JFrame implements ActionListener, KeyListener {

    JPanel p1;
    JTextField tNome;
    JButton bAdd, Limpar;
    JLabel lNome;
    JList lista;
    DefaultListModel<String> modelo;//lista de modelo padrão, vázio.
    JScrollPane barRolagem;

    public Produto() {

        p1 = new JPanel();
        tNome = new JTextField(20);
        bAdd = new JButton("Adicionar");
        Limpar = new JButton("Limpar");
        lNome = new JLabel("Nome");
        modelo = new DefaultListModel();
        lista = new JList(modelo);
        barRolagem = new JScrollPane(lista);

        p1 = new JPanel();
        p1.add(lNome);
        p1.add(tNome);
        p1.add(bAdd);
        p1.add(Limpar);
        p1.add(barRolagem);
        add(p1);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);/*-->
         finaliza a execução ao fechar a janela
         */

        setSize(400, 300);// Tamanho da janela
        setVisible(true);// exibe a janela
        setLocationRelativeTo(null);//Abri no centro da tela
        setResizable(false);//não redimencionar
        bAdd.addActionListener(this);//Escutar o botão
        Limpar.addActionListener(this);
        tNome.addKeyListener(this);//adicionar um botão do teclado

    }

    public static void main(String[] args) {
        new Produto();
    }

    public void adicionar() {
        modelo.addElement(tNome.getText());
        tNome.setText(null);
        tNome.requestFocus();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bAdd) {
            adicionar();
        }
        if (e.getSource() == Limpar) {
            tNome.setText(null);

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if(e.getKeyCode()== KeyEvent.VK_ENTER){//ação para acionar o teclado
            adicionar();
        }
        if(e.getKeyCode()== KeyEvent.VK_ESCAPE){
             tNome.setText(null);
        }
        
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}