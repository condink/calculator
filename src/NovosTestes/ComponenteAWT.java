package NovosTestes;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javafx.scene.control.CheckBox;
import javax.swing.*;

public class ComponenteAWT extends JFrame implements WindowListener {

    //Botão
    private Button botao = new Button("INSOFT");

    //Checkbox = Botão de seleção
    private Checkbox Checkbox = new Checkbox("Selecione esta caixa");

    // Os botões de opção podem ser organizados em grupos
    private CheckboxGroup grupoDeOpcao = new CheckboxGroup();

    /*Choice - caixa de seleção. Observe que o choice será inicializados
     posteriormente, pois depende de vários comandospara ser construido.
     */
    private Choice choice = null;

    /*
     List- uma lista de opções, semelhante ao choice porém com
     a caixa de opções fixa. Este componente também não pode ser
     totalmente construído totalmente com uma linha de código.
     */
    private List list = null;

    /*
     Label - Um componete não editavel de texto, usado normalmente 
     como titulo de outros textos.
     */
    private Label label = new Label("Digite seu nome");

    /*
     TextField - Campo de entrada de textos.
     */
    private TextField nome = new TextField();

    /*
     TextArea - área para edição de textos.
     */
    private TextArea texto = new TextArea("");

    /*
     Panel - um painel que pode ter componentes gráficos.
     */
    private Panel painel1 = new Panel();
    private Panel painel2 = new Panel();

    static public void main(String[] args) {

        ComponenteAWT gui = new ComponenteAWT();

        gui.setSize(400, 300);
        gui.setVisible(true);
        gui.addWindowListener(gui);

    }

    // Construtur de interface gráfica
    ComponenteAWT() {
        super("Desenvolvimento de teste da capacidade");

        // Inicializando um componente choice
        choice = new Choice();
        choice.add("Segunda");
        choice.add("Terça");
        choice.add("Quarta");
        choice.add("Quinta");
        choice.add("Sexta");

        /*Inicializando um componete List: Você diz o 
         número de opções que devem aparecer e se
         o componete aceita seleção múltipla
         */
        list = new List(3, false);
        list.add("Brasil");
        list.add("Canada");
        list.add("Chile");
        list.add("Dinamarca");
        list.add("Espanha");

        painel1.setLayout(new GridLayout(2, 2, 3, 3));
        painel1.add(botao);
        painel1.add(list);
        painel1.add(texto);

        //Exemplo de alinhamento de containers
        Panel conteiner1 = new Panel();
        conteiner1.setLayout(new FlowLayout());
        conteiner1.add(new Checkbox("Iniciante", grupoDeOpcao, true));
        conteiner1.add(new Checkbox("Avançado", grupoDeOpcao, false));
        
        Panel conteiner2 = new Panel();
        conteiner2.setLayout(new GridLayout(2,1));
        conteiner2.add(Checkbox);
        conteiner2.add(conteiner1);//Veja um conteiner dentro de outro
        
        painel1.add(conteiner2);
        
        painel2.setLayout(new BorderLayout(2, 2));
        
        
        
        
        
        
        
        
        
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
