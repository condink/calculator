
package NovosTestes;

import java.awt.BorderLayout;
import static java.awt.BorderLayout.CENTER;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class NovoCadastro extends JFrame implements ActionListener{
    
    JPanel p1,p2;
    JLabel l1,l2,l3,l4;
    JTextField Camp1, Camp2, Camp3, Camp4;
    JButton Enviar, Limpar,Relatorio, Cor1, Cor2;
    DefaultListModel<String> modelo;
    JScrollPane barRolagem;
    JList lista;
    
    public NovoCadastro(){
        
        p1 = new JPanel();
        p2 = new JPanel();
        
        l1 = new JLabel("Qual é o seu nome?");
        l2 = new JLabel("Qual o seu Email?");
        l3 = new JLabel("Telefone:");
        l4 = new JLabel("Data:");
        
        Camp1 = new JTextField(8);
        Camp2 = new JTextField(8);
        Camp3 = new JTextField(8);
        Camp4 = new JTextField(8);
        
        Enviar = new JButton("Enviar");
        Limpar = new JButton("Limpar");
        Relatorio = new JButton("Relatório");
        Cor1 = new JButton("Azul");
        Cor2 = new JButton("Branco");
        
        
        modelo = new DefaultListModel();
        lista = new JList(modelo);
        barRolagem = new JScrollPane(lista);
        
        p1.setLayout(new GridLayout(5,2));
        p1.add(l1);
        p1.add(Camp1);
        p1.add(l2);
        p1.add(Camp2);
        p1.add(l3);
        p1.add(Camp3);
        p1.add(l4);
        p1.add(Camp4);
        
        
        p2.add(Enviar);
        p2.add(Limpar);
        p2.add(Relatorio);
        p2.add(Cor1);
        p2.add(Cor2);
        p2.add(barRolagem);
        add(p2);
       
        
        setLayout(new BorderLayout());
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
       
        setSize(600,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
        
        Limpar.addActionListener(this);
        Relatorio.addActionListener(this);
        Enviar.addActionListener(this);
        Cor1.addActionListener(this);
        Cor2.addActionListener(this);
        
    }
    
    public static void main(String [] args){
        new NovoCadastro ();
    }
    public void adicionar(){
        modelo.addElement(Camp1.getText());
        Camp1.setText(null);
        modelo.addElement(Camp2.getText());
        Camp2.setText(null);
        modelo.addElement(Camp3.getText());
        Camp3.setText(null);
        modelo.addElement(Camp4.getText());
        Camp4.setText(null);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Enviar){
            adicionar();
        }
        
        if (e.getSource() == Relatorio){
            JOptionPane.showMessageDialog(null,"Nome: " + Camp1 + "E-mail: "
                    + Camp2 +"Fone: " + Camp3 + "Data: " +Camp4);
        }
        
        
        if (e.getSource() == Limpar){
            Camp1.setText(null);
            Camp2.setText(null);
            Camp3.setText(null);
            Camp4.setText(null);
            
        
            
        }
        if (e.getSource() == Cor1){
             p1.setBackground( Color.BLUE ); 
        }
         if (e.getSource() == Cor2){
             p1.setBackground( Color.WHITE ); 
        }
        
        
    }
    
    
    
    
    
    
}
