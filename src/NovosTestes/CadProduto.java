/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NovosTestes;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.lang.Thread.State.NEW;
import javax.swing.*;


public class CadProduto extends JFrame implements ActionListener, KeyListener {
    
    
    JPanel p1, p2;
    JLabel l1, l2, l3 ,l4;
    JTextField tCod, tProd, tQuant, tPrec;
    JButton bEnviar, bLimpar;
    Produto novoProduto;
    
    public CadProduto(){
        
        p1 = new JPanel();
        p2 = new JPanel();
        
        tCod = new JTextField(15);
        tProd = new JTextField(15);
        tQuant = new JTextField(15);
        tPrec = new JTextField(15);
        
        l1 = new JLabel("Código");
        l2 = new JLabel("Produto");
        l3 = new JLabel("Quantidade");
        l4 = new JLabel("Preço");
        
        bEnviar = new JButton("Enviar");
        bLimpar = new JButton("Limpar");
        
        //Criando as tabelas
        p1.setLayout (new GridLayout(4,2));
        p1.add(l1);p1.add(tCod);
        p1.add(l2);p1.add(tProd);
        p1.add(l3);p1.add(tQuant);
        p1.add(l4);p1.add(tPrec);
        
        p2.add(bEnviar);
        p2.add(bLimpar);
        
        setLayout(new BorderLayout());
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        
        setSize(500,250);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Cadastro de Produto");//Coloca o titulo na página
        pack();//Compactar a janela
        bLimpar.addActionListener(this);
        
    }
    public static void main(String [] args){
        new CadProduto ();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bLimpar){
            tCod.setText(null);
            tProd.setText(null);
            tQuant.setText(null);
            tPrec.setText(null);
        }
        if(e.getSource() == bEnviar){
            
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
       
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
    }
    
}