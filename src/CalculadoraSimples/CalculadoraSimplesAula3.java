package CalculadoraSimples;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class CalculadoraSimples extends JFrame implements ActionListener {

    JPanel p1, p2;
    JLabel l1, l2, l3;
    JTextField tNum1, tNum2, tResult;
    JButton Bsoma, bSub, bMult, bDiv, bRaiz, bLimpar;

    public CalculadoraSimples() {

        //Instancia de todos as variáveis
        p1 = new JPanel();
        p2 = new JPanel();

        l1 = new JLabel("1° Número:");
        l2 = new JLabel("2° Número:");
        l3 = new JLabel("Resultado:");

        tNum1 = new JTextField(7);
        tNum2 = new JTextField(7);
        tResult = new JTextField(7);

        Bsoma = new JButton("+");
        bSub = new JButton("-");
        bMult = new JButton("*");
        bDiv = new JButton("/");
        bRaiz = new JButton("Raiz");
        bLimpar = new JButton("Clear");

        //Gerenciador de layout GridLayout
        //Divide em linha e colunas
        //neste caso 3 liha e 2 colunas
        p1.setLayout(new GridLayout(3, 2));
        p1.add(l1);
        p1.add(tNum1);
        p1.add(l2);
        p1.add(tNum2);
        p1.add(l3);
        p1.add(tResult);

        p2.add(Bsoma);
        p2.add(bSub);
        p2.add(bMult);
        p2.add(bDiv);
        p2.add(bRaiz);
        p2.add(bLimpar);

        //gerenciador de layout BorderLayout divide em 5 regiões
        //NORTH - CENTER - SOUTH - WEST - EAST 
        setLayout(new BorderLayout());

        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);

        pack();//compactar a janela para ajustar o tamanho
        setLocationRelativeTo(null);//centraliza no meio do monitor
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//FINALIZA A JANELA
        setVisible(true);//torna a janela visivel

        Bsoma.addActionListener(this);
        bSub.addActionListener(this);
        bMult.addActionListener(this);
        bDiv.addActionListener(this);
        bRaiz.addActionListener(this);
        bLimpar.addActionListener(this);
    }//fim do construtor

    public static void main(String args[]) {
        new CalculadoraSimples();
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("teste");
        if (e.getSource() == Bsoma) {
            calcular('+');
        }
        if (e.getSource() == bSub) {
            calcular('-');
        }
        if (e.getSource() == bMult) {
            calcular('*');
        }
        if (e.getSource() == bDiv) {
            calcular('/');
        }
        if (e.getSource() == bRaiz) {
            calcular('R');
        }
        if (e.getSource() == bLimpar) {
            limparCampos();
        }
        //fim actionListener
    }

    public void limparCampos() {
        tNum1.setText(null);
        tNum2.setText(null);
        tResult.setText(null);
        tNum1.requestFocus();// focus do cursor      

    }// fim limparCampos

    public void calcular(char op) {
        double n1, n2, result = 0;
        try {

            n1 = Double.parseDouble(tNum1.getText());
            n2 = Double.parseDouble(tNum2.getText());

            switch (op) {
                case '+':
                    result = n1 + n2;
                    break;
                case '-':
                    result = n1 - n2;
                    break;
                case '*':
                    result = n1 * n2;
                    break;
                case '/':
                    result = n1 / n2;
                    break;
                case 'R':
                    result = Math.sqrt(n1);
                    break;
            }
            tResult.setText(String.valueOf(result));
        } catch (NumberFormatException erro) {
            JOptionPane.showMessageDialog(null, "Número invalido");
        }

    }
}
